package startit.rest.ex1.model;

public class Customer {
    private String name;
    private Integer id;
    private int age;

    public Customer(){}

    public Customer(String name, Integer id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String custDetails = String.format("Customer Info: name = %s, age = %d, id = %d",
                name, age, id);
        return custDetails;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
