package startit.rest.ex1.controller;

import org.springframework.web.bind.annotation.*;
import startit.rest.ex1.model.Customer;


@RestController
public class WebController {

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String post(@RequestBody Customer cust){
        System.out.println("/POST request, cust" + cust.toString());
        return "/Post successful";
    }

    @RequestMapping(value="/get", method=RequestMethod.GET)
    public Customer get(@RequestParam("name") String name, @RequestParam("age") int age, @RequestParam("id") Integer id){
        String info = String.format("/GET info: name=%s, age=%d, id=%d", name,age, id);
        System.out.println(info);
        return new Customer(name,age, id);
    }

    @RequestMapping(value="/put/{id}", method=RequestMethod.PUT)
    public void put(@PathVariable(value="id") Integer id, @RequestBody Customer cust){
        String info = String.format("id = %d, custinfo = %s", id, cust.toString());
        System.out.println("/PUT info " + info);
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable(value="id") Integer id){
        String info = String.format("/Delete info: id = %d", id);
        System.out.println(info);
    }
}
